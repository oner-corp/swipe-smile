// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import {MiniApp} from 'aq-miniapp';
import View from './views/View';
import './index.css';

function generateShouldWin() {
  return Math.random() > 0.5;
}

function generateUrl() {
  return Math.random() > 0.5
    ? 'https://i.imgur.com/U0DuYde.png'
    : 'https://i.imgur.com/u0QFX3A.png';
}

ReactDOM.render(
  <MiniApp
    join={View}
    data={{
      shouldWin: generateShouldWin(),
      winImageUrl: generateUrl(),
    }}
    devt={true}
  />,
  document.getElementById('root')
);
