// @flow
import React, {Component} from 'react';
import type {Output} from '../Types';
import '../css/View2.css';
import {GameComponent, GameStateRunning} from "../components/GameComponent";

export type Props = {
  onClick: (Output) => void,
  shouldWin: boolean,
  winImageUrl: string
};

export class View2 extends Component {
  state: {
    output: Output
  };

  constructor(props: Props) {
    super(props);

    this.state = {
      output: {}
    }
  }

  generateNumberOfSwipes() {
    let min;
    let max;
    if (this.props.shouldWin) {
      min = 15;
      max = 50;
    } else {
      min = 3;
      max = 30;
    }

    return Math.floor(Math.random() * (max - min)) + min;
  }

  render() {
    console.log(this.props.winImageUrl);
    return (
      <div>
        <GameComponent gameState={GameStateRunning}
                       onGameRestarted={() => this.props.onClick()}
                       requiredNumberOfSwipes={this.generateNumberOfSwipes()}
                       calculateGameResults={() => this.props.shouldWin}
                       winImageUrl={this.props.winImageUrl}
        />
      </div>
    )
  }
}
