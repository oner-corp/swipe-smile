// @flow
import React, {Component} from 'react';
import '../css/View.css';
import {GameComponent, GameStateNotStarted} from "../components/GameComponent";

export class View1 extends Component {
  render() {
    return (
      <div>
        <GameComponent gameState={GameStateNotStarted} onGameStarted={() => this.props.onClick()}/>
      </div>
    );
  }
}
