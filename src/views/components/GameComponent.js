// @flow
import '../css/GameComponentCss.css';
import React, {Component} from 'react';
import smileImg from '../../assets/smile.png'
import bombImg from '../../assets/bomb.png'
import emptySmileImg from '../../assets/empty_simle.png'
import * as Pixi from 'pixi.js';
import {VectorUtils} from '../../utils/VectorUtils';

const CLOCKWISE = false;
const COUNTER_CLOCKWISE = true;
const WHITE = 0xFFFFFF;
const BLACK = 0x000000;
const BG_COLOR = 0xFF69B4;
const OPAQUE = 1;
const ANCHOR_CENTER = 0.5;
const SMILEYS_OFFSET = 10;
const SMILEYS_COUNT = 100;

/**
 * Indicates how much a user should swipe an object to apply swipe.
 * 1 means that user should swipe the whole object
 * 0.75 means that user should swipe at least 75% of the object
 * @type {number}
 */
const MIN_SWIPE_RATIO = 0.75;

export type GameState = 'Not started' | 'Running' | 'Completed: Win' | 'Completed: Lose';
export const GameStateNotStarted: GameState = 'Not started';
export const GameStateRunning: GameState = 'Running';
export const GameStateCompletedWin: GameState = 'Completed: Win';
export const GameStateCompletedLose: GameState = 'Completed: Lose';

export type GameComponentProps = {
  gameState: GameState,
  onGameStarted?: () => void,
  onGameCompleted?: (GameState) => void,
  onGameRestarted?: () => void,
  requiredNumberOfSwipes?: number,
  calculateGameResults?: () => boolean,
  winImageUrl?: string
};

export type Point = {
  x: number,
  y: number
}

export type AnimationState = {
  animateSmileyRemovingFromStack?: boolean,
  animateSmileyReturningToStack?: boolean,
  animateSmileyStackMoving?: boolean,

  removeTopSmiley?: boolean,
  finishAllAnimations?: boolean,

  startPoint?: Point,
  currentPoint?: Point,
  targetStackX?: number,
  targetStackY?: number,

  updateWinImageScale?: boolean,
}

export class GameComponent extends Component {
  animationState: AnimationState;
  props: GameComponentProps;

  constructor(props: GameComponentProps) {
    super(props);

    this.app = null;
    this.gameCanvas = null;
    this.smiley = null;
    this.smileyMask = null;
    this.smileyContainer = null;
    this.mirroredSmiley = null;
    this.mirroredSmileyMask = null;
    this.mirroredSmileyContainer = null;
    this.largeSmileysContainer = null;
    this.isDragging = null;
    this.animationState = null;
    this.startPoint = null;
    this.totalSwipes = 0;
    this.winSmileyTexture = null;
    this.loseSmileyTexture = null;
    this.emptySmileTexture = null;
  }

  componentDidMount() {
    this.app = new Pixi.Application(window.innerWidth, window.innerHeight,
      {antialias: true, backgroundColor: BG_COLOR});
    this.gameCanvas.appendChild(this.app.view);

    this.animationState = Object();

    const smileyTexture = Pixi.Texture.fromImage(smileImg);
    this.loadWinSmileyTexture();
    this.loseSmileyTexture = Pixi.Texture.fromImage(bombImg);
    this.emptySmileTexture = Pixi.Texture.fromImage(emptySmileImg);
    this.createSmileysStack(smileyTexture);

    this.createSmiley(smileyTexture);
    this.createSmileyMask();

    this.createMirroredSmiley(smileyTexture);
    this.createMirroredSmileyMask();

    this.app.ticker.add(this.handleTick.bind(this));

    if (this.props.gameState === GameStateNotStarted) {
      this.createLabel('Swipe smiley', 1000);
    }
    else if (this.props.gameState === GameStateRunning) {
      this.createLabel('Keep swiping');
    }
    else if (this.props.gameState === GameStateCompletedWin) {
      this.smiley.texture = this.winSmileyTexture;
      this.animationState.updateWinImageScale = true;
      this.createLabel('You won!');
      this.createNextButton(this.props.onGameRestarted);
    }
    else if (this.props.gameState === GameStateCompletedLose) {
      this.smiley.texture = this.loseSmileyTexture;
      this.createLabel('Sorry, you lose');
      this.createNextButton(this.props.onGameRestarted);
    }

    if (this.props.gameState !== GameStateRunning) {
      this.largeSmileysContainer.visible = false;
    }

    this.app.start();
  }

  loadWinSmileyTexture() {
    if (this.props.winImageUrl) {
      const crossOrigin = true;
      this.winSmileyTexture = Pixi.Texture.fromImage(this.props.winImageUrl, crossOrigin);
    } else {
      this.winSmileyTexture = null;
    }
  }

  createLabel(text: string, timeout?: number) {
    if (this.labelContainer) {
      this.app.stage.removeChild(this.labelContainer);
    }

    let labelPaddingBottom;
    if (this.app.screen.height > 500) {
      labelPaddingBottom = 100;
    } else {
      labelPaddingBottom = 75;
    }

    const labelContainer = new Pixi.Container();
    labelContainer.x = this.app.screen.width / 2;
    labelContainer.y = this.app.screen.height - labelPaddingBottom;
    const style = new Pixi.TextStyle({
      fill: WHITE,
      fontFamily: 'Canaro Medium',
      dropShadow: true,
      dropShadowColor: BLACK,
      dropShadowBlur: 4,
      dropShadowAngle: Math.PI / 6,
      dropShadowDistance: 2,
    });
    const labelText = new Pixi.Text(text, style);
    labelText.anchor.set(ANCHOR_CENTER);
    labelContainer.addChild(labelText);
    this.app.stage.addChild(labelContainer);

    this.labelContainer = labelContainer;

    if (timeout) {
      // hack to wait until font is loaded
      setTimeout(() => this.createLabel(text), 1000);
    }
  }

  createNextButton(handler: () => void) {
    let buttonPaddingBottom;
    if (this.app.screen.height > 500) {
      buttonPaddingBottom = 60;
    } else {
      buttonPaddingBottom = 35;
    }

    const nextButtonContainer = new Pixi.Container();
    nextButtonContainer.x = this.app.screen.width / 2;
    nextButtonContainer.y = this.app.screen.height - buttonPaddingBottom;
    const style = new Pixi.TextStyle({
      fill: WHITE,
      dropShadow: true,
      fontFamily: 'Canaro Medium',
      dropShadowColor: BLACK,
      dropShadowBlur: 4,
      dropShadowAngle: Math.PI / 6,
      dropShadowDistance: 2,
    });
    const nextButtonText = new Pixi.Text('NEXT', style);
    nextButtonText.anchor.set(ANCHOR_CENTER);
    nextButtonContainer.addChild(nextButtonText);
    this.app.stage.addChild(nextButtonContainer);
    nextButtonText.interactive = true;
    nextButtonText.buttonMode = true;
    nextButtonText
      .on('pointerup', () => {
        console.log('restarted');
        handler();
      });
  }

  createMirroredSmileyMask() {
    this.mirroredSmileyMask = new Pixi.Graphics();
    this.app.stage.addChild(this.mirroredSmileyMask);
  }

  createMirroredSmiley(smileyTexture) {
    this.mirroredSmileyContainer = new Pixi.Container();
    this.mirroredSmileyContainer.x = 0;
    this.mirroredSmileyContainer.y = 0;
    this.mirroredSmileyContainer.visible = false;

    this.mirroredSmiley = new Pixi.Sprite(smileyTexture);
    this.mirroredSmiley.anchor.set(ANCHOR_CENTER);
    this.mirroredSmileyContainer.addChild(this.mirroredSmiley);
    this.app.stage.addChild(this.mirroredSmileyContainer);
  }

  createSmileyMask() {
    this.smileyMask = new Pixi.Graphics();
    this.app.stage.addChild(this.smileyMask);
  }

  createSmiley(smileyTexture) {
    this.smileyContainer = new Pixi.Container();
    this.smileyContainer.x = this.app.screen.width / 2;
    this.smileyContainer.y = this.app.screen.height / 2;

    this.smiley = new Pixi.Sprite(smileyTexture);
    this.smiley.interactive = true;
    this.smiley.anchor.set(ANCHOR_CENTER);

    this.smileyContainer.addChild(this.smiley);

    this.app.stage.addChild(this.smileyContainer);

    this.smiley
      .on('pointerdown', e => this.onDragStart(e))
      .on('pointerup', e => this.onDragEnd(e))
      .on('pointerupoutside', e => this.onDragEnd(e))
      .on('pointermove', e => this.onDragMove(e));
  }

  createSmileysStack(smileyTexture) {
    this.largeSmileysContainer = new Pixi.Container();
    let totalSmileysCount;
    if (this.props.gameState === GameStateRunning) {
      totalSmileysCount = SMILEYS_COUNT;
    } else {
      // do not create smileys stack at all if state is not running
      totalSmileysCount = 0;
    }

    for (let i = 0; i < totalSmileysCount; i++) {
      const smileyIndex = totalSmileysCount - i;
      let sm;
      if (this.props.requiredNumberOfSwipes === smileyIndex) {
        // displaying empty image here, real image will be loaded later
        sm = new Pixi.Sprite(this.emptySmileTexture);
      } else {
        sm = new Pixi.Sprite(smileyTexture);
      }

      sm.anchor.set(ANCHOR_CENTER);
      sm.x = smileyIndex * SMILEYS_OFFSET;
      sm.y = -smileyIndex * SMILEYS_OFFSET;
      this.largeSmileysContainer.addChild(sm);

      if (this.props.requiredNumberOfSwipes === smileyIndex) {
        let sprite;
        const isWin = this.props.calculateGameResults();
        console.log(isWin);
        if (isWin) {
          sprite = new Pixi.Sprite(this.winSmileyTexture);
          this.winSmileySprite = sprite;
          sprite.visible = false;
          this.animationState.updateWinImageScale = true;

          const spriteMask = new Pixi.Sprite(this.emptySmileTexture);
          spriteMask.scale.x = 0.8;
          spriteMask.scale.y = 0.8;
          spriteMask.anchor.set(ANCHOR_CENTER);
          spriteMask.x = smileyIndex * SMILEYS_OFFSET;
          spriteMask.y = -smileyIndex * SMILEYS_OFFSET;
          this.largeSmileysContainer.addChild(spriteMask);
          sprite.mask = spriteMask;
        } else {
          sprite = new Pixi.Sprite(this.loseSmileyTexture);
        }

        sprite.anchor.set(ANCHOR_CENTER);
        sprite.x = smileyIndex * SMILEYS_OFFSET;
        sprite.y = -smileyIndex * SMILEYS_OFFSET;
        this.largeSmileysContainer.addChild(sprite);
      }
    }

    this.largeSmileysContainer.x = this.app.screen.width / 2;
    this.largeSmileysContainer.y = this.app.screen.height / 2;
    this.app.stage.addChild(this.largeSmileysContainer);
  }

  componentWillUnmount() {
    this.app.stop();
  }

  static hasSize(sprite) {
    return sprite != null && sprite.height > 1;
  }

  updateWinImageScale() {
    console.log('updateWinImageScale');
    if (!GameComponent.hasSize(this.emptySmileTexture)) {
      return;
    }
    if (!GameComponent.hasSize(this.winSmileyTexture)) {
      return;
    }

    const xScale = this.emptySmileTexture.height / this.winSmileyTexture.height;
    const yScale = this.emptySmileTexture.width / this.winSmileyTexture.width;

    if (this.props.gameState === GameStateCompletedWin) {
      this.smiley.scale.x = Math.min(xScale, yScale) * 0.8;
      this.smiley.scale.y = Math.min(xScale, yScale) * 0.8;
      this.smiley.visible = true;
    } else {
      this.winSmileySprite.scale.x = Math.min(xScale, yScale) * 0.8;
      this.winSmileySprite.scale.y = Math.min(xScale, yScale) * 0.8;
      this.winSmileySprite.visible = true;
    }

    this.animationState.updateWinImageScale = false;
  }

  handleTick(delta: number) {
    if (this.isDragging) {
      return;
    }

    if (this.animationState.updateWinImageScale) {
     this.updateWinImageScale();
    }

    if (this.animationState.removeTopSmiley) {
      const largeSmileysContainer = this.largeSmileysContainer;

      const lastChild = largeSmileysContainer.children[largeSmileysContainer.children.length - 1];
      if (lastChild) {
        largeSmileysContainer.removeChild(lastChild);
      }
      this.animationState.removeTopSmiley = false;
    }

    if (this.animationState.finishAllAnimations) {
      this.applyAllAnimationsFinishedTick();
    }
    if (this.animationState.animateSmileyReturningToStack) {
      this.applySmileyReturningToStackAnimationTick(delta);
    }
    if (this.animationState.animateSmileyRemovingFromStack) {
      this.applySmileyRemovingFromStackAnimationTick(delta);
    }
    if (this.animationState.animateSmileyStackMoving) {
      this.applySimleyStackMovingAnimationTick(delta);
    }
  }

  applySimleyStackMovingAnimationTick(delta: number) {
    const largeSmileysContainer = this.largeSmileysContainer;

    largeSmileysContainer.position.x -= 2 * delta;
    if (largeSmileysContainer.position.x < this.animationState.targetStackX) {
      largeSmileysContainer.position.x = this.animationState.targetStackX;
    }
    largeSmileysContainer.position.y += 2 * delta;
    if (largeSmileysContainer.position.y > this.animationState.targetStackY) {
      largeSmileysContainer.position.y = this.animationState.targetStackY;
    }

    if (this.animationState.targetStackX === largeSmileysContainer.position.x &&
      this.animationState.targetStackY === largeSmileysContainer.position.y) {
      this.animationState.finishAllAnimations = true;
      if (this.totalSwipes < this.props.requiredNumberOfSwipes) {
        this.animationState.removeTopSmiley = true;
      }
    }
  }

  applySmileyRemovingFromStackAnimationTick(delta: number) {
    const startPoint = this.animationState.startPoint;
    const currentPoint = this.animationState.currentPoint;
    if (startPoint == null || currentPoint == null) {
      this.finishSmileyRemovingFromStackAnimation();
      this.startStackMovingAnimation();
      return;
    }

    const swipeVector = VectorUtils.toVector(startPoint, currentPoint);
    const vecLen = VectorUtils.getLength(swipeVector);
    if (vecLen > this.smiley.width * 2) {
      this.finishSmileyRemovingFromStackAnimation();
      this.startStackMovingAnimation();
    } else {
      const animationLength = 10 * delta;
      const newVecLen = vecLen + animationLength;
      const newVec = VectorUtils.setVectorLength(swipeVector, newVecLen);
      const newCurrentPoint = VectorUtils.movePointByVector(startPoint, newVec);
      this.handleMove(newCurrentPoint);
      this.animationState.currentPoint = newCurrentPoint;
    }
  }

  finishSmileyRemovingFromStackAnimation() {
    this.mirroredSmileyContainer.visible = false;
    this.animationState.animateSmileyRemovingFromStack = false;
    this.onSmileySwiped();
  }

  startStackMovingAnimation() {
    this.animationState.animateSmileyStackMoving = true;
    this.animationState.targetStackX = this.largeSmileysContainer.position.x - SMILEYS_OFFSET;
    this.animationState.targetStackY = this.largeSmileysContainer.position.y + SMILEYS_OFFSET;
  }

  applySmileyReturningToStackAnimationTick(delta: number) {
    const minSwipeLen = 20;

    const startPoint = this.animationState.startPoint;
    const currentPoint = this.animationState.currentPoint;
    if (startPoint == null || currentPoint == null) {
      this.animationState.finishAllAnimations = true;
      return;
    }

    const swipeVector = VectorUtils.toVector(startPoint, currentPoint);
    const vecLen = VectorUtils.getLength(swipeVector);
    if (vecLen < minSwipeLen) {
      this.animationState.finishAllAnimations = true;
    } else {
      const animationLength = 10 * delta;
      const newVecLen = vecLen - animationLength;
      const newVec = VectorUtils.setVectorLength(swipeVector, newVecLen);
      const newCurrentPoint = VectorUtils.movePointByVector(startPoint, newVec);
      this.handleMove(newCurrentPoint);
      this.animationState.currentPoint = newCurrentPoint;
    }
  }

  applyAllAnimationsFinishedTick() {
    this.animationState = {};
    this.startPoint = {};
    this.smileyMask.clear();
    this.smiley.mask = null;
    this.mirroredSmileyContainer.visible = false;
  }

  isEmpty(obj) {
    for (const key in obj) {
      if (this.hasOwnProperty(key)) {
        return false;
      }
    }
    return true;
  }

  isAnimating() {
    return !this.isEmpty(this.animationState);
  }

  onDragStart(event) {
    if (this.isAnimating() || this.isDragging) {
      return;
    }

    this.isDragging = true;
    this.startPoint = {
      x: event.data.global.x,
      y: event.data.global.y
    };
    this.smiley.mask = null;
  }

  onDragEnd(event) {
    if (this.isAnimating() || !this.isDragging) {
      return;
    }
    if (this.props.gameState === GameStateCompletedWin || this.props.gameState === GameStateCompletedLose) {
      return;
    }

    const swipeLen = GameComponent.getSmileySwipeLength(this.startPoint, event.data.global);
    if (swipeLen > this.smiley.width * MIN_SWIPE_RATIO) {
      this.animationState.animateSmileyRemovingFromStack = true;
    } else {
      this.animationState.animateSmileyReturningToStack = true;
    }
    this.animationState.startPoint = {
      x: this.startPoint.x,
      y: this.startPoint.y
    };
    this.animationState.currentPoint = {
      x: event.data.global.x,
      y: event.data.global.y
    };
    this.isDragging = false;
  }

  static getSmileySwipeLength(startPoint, currentPoint) {
    const v = VectorUtils.toVector(startPoint, currentPoint);
    return VectorUtils.getLength(v);
  }

  static updateMask(mask, smileyContainer, smiley, radius, arcAngle, anticlockwise, rotationAngle) {
    mask.position.x = smileyContainer.position.x;
    mask.position.y = smileyContainer.position.y;

    let startAndle = arcAngle;
    let endAngle = 2 * Math.PI - arcAngle;

    mask.clear();
    mask.beginFill(WHITE, OPAQUE);
    mask.arc(0, 0, radius, startAndle, endAngle, anticlockwise);
    mask.rotation = VectorUtils.normalizeAngle(rotationAngle);

    smiley.mask = mask;
  }

  updateSmileyMasks(startPoint, currentPoint) {
    // the function consists of 2 parts:
    // 1. Draw mask arc to hide moved part
    // 2. Rotate it to handle user swipe direction

    // prepare mask arc parameters
    let len = GameComponent.getSmileySwipeLength(startPoint, currentPoint);
    let radius = this.smiley.width / 2;
    let angle = Math.acos((radius - len / 2) / radius);
    if (isNaN(angle)) {
      angle = 2 * Math.PI;
    }

    // prepare mask rotation
    let v1 = {x: radius, y: 0};
    let v2 = VectorUtils.toVector(startPoint, currentPoint);
    let rotationAngle = VectorUtils.getAngleBetweenVectors(v1, v2);

    GameComponent.updateMask(this.smileyMask, this.smileyContainer, this.smiley,
      radius, angle, CLOCKWISE, rotationAngle + Math.PI);
    GameComponent.updateMask(this.mirroredSmileyMask, this.mirroredSmileyContainer, this.mirroredSmiley,
      radius, angle, COUNTER_CLOCKWISE, rotationAngle);
  }

  updateMirroredSmiley(startPoint, currentPoint) {
    const swipeVector = VectorUtils.toVector(startPoint, currentPoint);
    const len = VectorUtils.getLength(swipeVector);
    const radius = this.smiley.width / 2;
    const delta = radius - len / 2;
    const vectorToMirroredSmileyPosition = VectorUtils.setVectorLength(swipeVector, -delta * 2);
    const smileyPosition = {
      x: this.smileyContainer.x,
      y: this.smileyContainer.y
    };
    const mirroredSmileyLocation = VectorUtils.movePointByVector(smileyPosition, vectorToMirroredSmileyPosition);

    this.mirroredSmileyContainer.position.x = mirroredSmileyLocation.x;
    this.mirroredSmileyContainer.position.y = mirroredSmileyLocation.y;

    const v1 = {x: radius, y: 0};
    const v2 = VectorUtils.toVector(startPoint, currentPoint);
    const rotationAngle = VectorUtils.getAngleBetweenVectors(v1, v2);
    this.mirroredSmiley.rotation = VectorUtils.normalizeAngle(rotationAngle * 2);
  }

  handleMove(currentPoint) {
    this.mirroredSmileyContainer.visible = true;
    this.updateMirroredSmiley(this.startPoint, currentPoint);
    this.updateSmileyMasks(this.startPoint, currentPoint);
  }

  onDragMove(event) {
    if (this.isAnimating() || !this.isDragging) {
      return;
    }
    if (this.props.gameState === GameStateCompletedLose || this.props.gameState === GameStateCompletedWin) {
      return;
    }

    const currentPoint = event.data.global;
    this.handleMove(currentPoint);
  }

  onSmileySwiped() {
    if (this.props.gameState === GameStateNotStarted) {
      this.props.onGameStarted()
    } else if (this.props.gameState === GameStateRunning) {
      this.totalSwipes += 1;
      if (this.totalSwipes >= this.props.requiredNumberOfSwipes) {
        const isWin = this.props.calculateGameResults();
        this.handleGameCompleted(isWin);
      }
    }
  }

  handleGameCompleted(isWin) {
    this.smiley.visible = false;

    if (isWin) {
      this.createLabel('You won!');
      this.createNextButton(this.props.onGameRestarted);
    } else {
      this.createLabel('Sorry, you lose');
      this.createNextButton(this.props.onGameRestarted);
    }
  }

  render() {
    const component = this;
    return (
      <div ref={(thisDiv) => {
        component.gameCanvas = thisDiv
      }}/>
    );
  }
}
