// @flow
import React, {Component} from 'react';
import {CloudStorage, defaultLifeCycle, StaticCanvas} from 'aq-miniapp';
import {View1, View2, View3,} from './js';

import type {Output} from './Types';

type Props = {
  cloudStorageClient: CloudStorage,
  id?: string,
  data?: Object,
  mode: 'preview' | 'join'
}

export default class View extends Component {
  state: {
    currentPage: number,
    output: Output,
    data: ?Object,
    mode: 'preview' | 'join',
  }

  constructor(props: Props) {
    super(props);
    this.state = {
      currentPage: 1,
      output: {},
      data: props.data,
      mode: props.mode
    }
  }

  _onView1Click() {
    this.setState({currentPage: 2});
  }

  _onView2Click(output: Output) {
    this.setState({currentPage: 3, output: output});
  }

  _onView3Click() {
    this.setState({currentPage: 1});
    // defaultLifeCycle.end();
  }

  render() {
    const height = window.innerWidth;
    const width = window.innerHeight;
    let render = <StaticCanvas width={width} height={height}/>

    switch (this.state.currentPage) {
      case 1:
        render = <View1 onClick={this._onView1Click.bind(this)}/>
        break;
      case 2:
        render = <View2 onClick={this._onView3Click.bind(this)}
                        shouldWin={this.props.data.shouldWin}
                        winImageUrl={this.props.data.winImageUrl}/>
        break;
      case 3:
        render = <View3 output={this.state.output}
                        winImageUrl={this.props.data.winImageUrl}
                        onClick={this._onView3Click.bind(this)}/>
        break;
      default:
        break;
    }
    // }
    return (
      <div className='container' style={{backgroundColor: "#FF69B4"}}>
        {render}
      </div>
    );
  }
}
